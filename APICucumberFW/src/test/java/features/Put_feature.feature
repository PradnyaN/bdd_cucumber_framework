Feature: Trigger Put_API 

Scenario: Trigger the  Put_API request with valid request parameters 
	Given Enter NAME and JOB in Put requestbody 
	When Send the Put request with payload 
	Then Validate Put status code 
	And Validate Put responsebody parameters