Feature: Trigger PatchAPI 

Scenario: Trigger the  PatchAPI request with valid request parameters 
	Given Enter NAME and JOB in Patchrequest body 
	When Send the Patch request with payload 
	Then Validate Patch status code 
	And Validate Patch response body parameters