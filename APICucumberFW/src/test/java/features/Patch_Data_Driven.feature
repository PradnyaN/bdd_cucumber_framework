Feature: Trigger PatchAPI 

Scenario Outline: Trigger the  PatchAPI request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in Patchrequest body 
	When Send the Patch request with Data
	Then Validate Data_driven Patch status code 
	And Validate Data_Driven Patch response body parameters
	
Examples:    
      |Name |Job |
      |Pranav |QA| 
      |Anuja |QA|
      |Apurva |QA|
      |Riva |Dev|	