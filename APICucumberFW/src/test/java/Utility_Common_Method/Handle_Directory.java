package Utility_Common_Method;

import java.io.File;

public class Handle_Directory {
	public static File create_log_directory(String log_dir) {
//Fectch the current project directory	
		String project_dir = System.getProperty("user.dir");
		System.out.println("current project directory path is : " + project_dir);
		File directory = new File(project_dir + "\\API_logs\\"+log_dir);

		if (directory.exists()) {
			directory.delete();
			System.out.println(directory + " :deleted");
			directory.mkdir();
			System.out.println(directory + " :created");
		} else {
			directory.mkdir();
			System.out.println(directory + " :created");
		}

		return directory;
	}

}