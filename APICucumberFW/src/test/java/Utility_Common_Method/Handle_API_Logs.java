package Utility_Common_Method;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_API_Logs {

	public static void evidence_creator(File dir_name, String File_name, String endpoint, String requestBody,
			String responseBody) throws IOException {
		File newFile = new File(dir_name + "\\" + File_name + ".txt");
		System.out.println("to save request and response body we have created a new file named : " + newFile.getName());

		FileWriter dataWriter = new FileWriter(newFile);
		dataWriter.write("Endpoint is :" + endpoint + "\n\n");
		dataWriter.write("Request Body is :" + requestBody + "\n\n");
		dataWriter.write("Response Body is :" + responseBody);
		dataWriter.close();
	}

}
