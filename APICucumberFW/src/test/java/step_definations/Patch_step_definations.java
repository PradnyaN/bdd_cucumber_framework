package step_definations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_method_handle_API;
import Endpoints.Patch_endpoint_testcase1;

import Request_repository.Patch_Request_repository;

import Utility_Common_Method.Handle_API_Logs;
import Utility_Common_Method.Handle_Directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Patch_step_definations {
	File log_dir;
	 String requestBody;
	 String endpoint;
	 String responsebody;
	 int statuscode;
	 @Given("Enter NAME and JOB in Patchrequest body")
	 public void enter_and_in_patchrequest_body(String req_name, String req_job) throws IOException {
		 log_dir = Handle_Directory.create_log_directory("post_tc1_logs");
		 requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
         endpoint = Patch_endpoint_testcase1.patch_endpoint_tc1();
	    
	 }
	 @When("Send the Patch request with Data")
	 public void send_the_patch_request_with_payload() {
		 statuscode = Common_method_handle_API.patch_statuscode(requestBody, endpoint);
		 responsebody= Common_method_handle_API.patch_responsebody(requestBody, endpoint);
		System.out.println(responsebody);
	    
	 }
	 @Then("Validate Data_driven Patch status code")
	 public void validate_patch_status_code() {
		 Assert.assertEquals(statuscode,200);
	    
	 }
	 @Then("Validate Data_Driven Patch response body parameters")
	 public void validate_patch_response_body_parameters() throws IOException {
			Handle_API_Logs.evidence_creator(log_dir, "patch_tc1", endpoint, requestBody, responsebody);

	 }



}
