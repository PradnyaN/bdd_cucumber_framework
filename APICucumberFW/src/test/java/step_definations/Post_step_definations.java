package step_definations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_method_handle_API;
import Endpoints.Post_endpoint;
import Request_repository.Post_request_repository;

import Test_package.post_tc1;
import Utility_Common_Method.Handle_API_Logs;
import Utility_Common_Method.Handle_Directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Post_step_definations extends Common_method_handle_API{
	 File log_dir;
	 String requestBody;
	 String endpoint;
	 String responsebody;
	 int statuscode;
	@Given("Enter NAME and JOB in Postrequest body")
	public void enter_name_and_job_in_postrequest_body() throws IOException {
		log_dir = Handle_Directory.create_log_directory("post_tc1_logs");
		 requestBody = Post_request_repository.post_request_testcase1();
		 endpoint = Post_endpoint.post_endpoint_testcase1();
	}
	@When("Send the Post request with payload")
	public void send_the_post_request_with_payload() {
		 statuscode = Common_method_handle_API.post_statuscode(requestBody, endpoint);
		 responsebody= Common_method_handle_API.post_responsebody(requestBody, endpoint);
		System.out.println(responsebody);
	}
	@Then("Validate Post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(statuscode, 201);
	}
	@Then("Validate Post response body parameters")
	public void validate_post_response_body_parameters() throws IOException {
		Handle_API_Logs.evidence_creator(log_dir,"post_tc1", endpoint, requestBody,responsebody);
post_tc1.Validator(requestBody, responsebody);
		System.out.println("post_API response is successful");

	}

}
