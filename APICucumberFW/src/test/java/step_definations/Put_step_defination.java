package step_definations;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import API_Common_Method.Common_method_handle_API;
import Endpoints.Put_endpoint_testcase1;
import Request_repository.Put_Request_repository;
import Test_package.Put_tc1;

import Utility_Common_Method.Handle_API_Logs;
import Utility_Common_Method.Handle_Directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Put_step_defination extends Common_method_handle_API{
	File log_dir;
	 String requestBody;
	 String endpoint;
	 String responsebody;
	 int statuscode;
	@Given("Enter NAME and JOB in Putrequest body")
	public void enter_name_and_job_in_putrequest_body() throws IOException {
		log_dir = Handle_Directory.create_log_directory("Put_tc1_logs");
		requestBody = Put_Request_repository.put_tc1();
		endpoint = Put_endpoint_testcase1.Put_endpoint_tc1();
	}
	@When("Send the Put request with payload")
	public void send_the_put_request_with_payload() {
		statuscode = Common_method_handle_API.put_statuscode(requestBody, endpoint);
		 responsebody= Common_method_handle_API.put_responsebody1(requestBody, endpoint);
		System.out.println(responsebody);
	}
	@Then("Validate Put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statuscode, 200);
	    
	}
	@Then("Validate Put response body parameters")
	public void validate_put_response_body_parameters() throws IOException {
		Handle_API_Logs.evidence_creator(log_dir,"Put_tc1",endpoint, requestBody,responsebody);  
		Put_tc1.validator(requestBody,responsebody);
		System.out.println("put_API response is successful");

	}



}
