package Request_repository;

import org.testng.annotations.DataProvider;

public class TestNG_Data_Provider {
	@DataProvider()
	public Object[][]Post_Data(){
		return new Object[][] {
			 {"Pradnya","QA"},
			 {"Pooja","QA"},
			 {"Neha","Manager"} ,
			 {"Amar","Lead"},
			 {"Aparna","QA"}
		};
	}
}
