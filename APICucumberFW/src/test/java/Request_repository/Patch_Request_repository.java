package Request_repository;

import java.io.IOException;
import java.util.ArrayList;

import Utility_Common_Method.New_excel;

public class Patch_Request_repository {

	public static String Patch_request_testcase1() throws IOException {
		ArrayList<String> Data = New_excel.Excel_Read_data("Testdata", "Patch_API", "Patch_tc 1");
		System.out.println(Data);
		String name = Data.get(1);
		String job = Data.get(2);
//String requestbody=	"{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
		String requestbody = "{\r\n" + "    \"name\": \"" + name + "\",\r\n" + "    \"job\": \"" + job + "\"\r\n" + "}";

		return requestbody;
	}
}
