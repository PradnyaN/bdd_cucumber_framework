package Test_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import API_Common_Method.Common_method_handle_API;
import Endpoints.Post_endpoint;
import Request_repository.Post_request_repository;
import Request_repository.TestNG_Data_Provider;
import Utility_Common_Method.Handle_API_Logs;
import Utility_Common_Method.Handle_Directory;
import io.restassured.path.json.JsonPath;

public class TestNG_Driven extends Common_method_handle_API {

	static File log_dir;
	static String requestBody;
	static String endpoint;
	static String responsebody;
	@DataProvider()
	public Object[][]Post_requestbody(){
		return new Object[][] {
			 {"Pradnya","QA"},
			 {"Pooja","QA"},
			 {"Neha","Manager"},
			 {"Amar","Lead"},
			 {"Aparna","QA"}
			 };
	}
	@BeforeTest
	public static void Test_setup() throws IOException {
		log_dir = Handle_Directory.create_log_directory("post_tc1_logs");
		 endpoint = Post_endpoint.post_endpoint_testcase1();
	}
	//@Test(dataProvider="Post_requestbody")
	@Test(dataProvider="Post_Data",dataProviderClass=TestNG_Data_Provider.class)

	public static void post_executor(String name ,String job) throws IOException {
		 requestBody = "{\r\n"
		 		+ "    \"name\": \""+name+"\",\r\n"
		 		+ "    \"job\": \""+job+"\"\r\n"
		 		+ "}";

		for (int i = 0; i < 3; i++) {
			int statuscode = post_statuscode(requestBody, endpoint);
			System.out.println(statuscode);
			if (statuscode == 201) {

				responsebody = post_responsebody(requestBody, endpoint);
				System.out.println(responsebody);
				
				post_tc1.Validator(requestBody, responsebody);
				break;
			} else {
				System.out.println("expected statuscode is not found hence retrying");
			}
		}
	}

	public static void Validator(String requestbody, String responsebody) {
		JsonPath jsp_req = new JsonPath(requestbody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responsebody);
		String res_name = jsp_res.getString("name");
		String res_id = jsp_res.getString("id");
		String res_job = jsp_res.getString("job");
		String res_createdate = jsp_res.getString("createdAt");
		res_createdate = res_createdate.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdate, expecteddate);
	}
	@AfterTest
	public static void Test_teardown() throws IOException {
		String post_tc1_log_file = post_tc1.class.getName();

		Handle_API_Logs.evidence_creator(log_dir, post_tc1_log_file, endpoint, requestBody, responsebody);
}
}

