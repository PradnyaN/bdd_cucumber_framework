Endpoint is :https://reqres.in/api/users

Request Body is :{
    "name": "morpheus",
    "job": "leader"
}

Response Body is :{"name":"morpheus","job":"leader","id":"692","createdAt":"2024-01-03T14:17:21.629Z"}