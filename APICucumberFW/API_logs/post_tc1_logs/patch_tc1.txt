Endpoint is :https://reqres.in/api/users/2

Request Body is :{
    "name": "morpheus",
    "job": "leader"
}

Response Body is :{"name":"morpheus","job":"leader","updatedAt":"2024-01-03T14:08:38.474Z"}