Feature: Trigger PostAPI 

Scenario Outline: Trigger the  PostAPI request with valid request parameters 
	Given Enter "<Name>" and "<Job>" in Postrequest body 
	When Send the Post request with data
	Then Validate data driven_Post status code 
	And Validate data driven_Post response body parameters
	
Examples:    
      |Name |Job |
      |Pradnya|QA| 
      |Aditi|QA|
      |Pooja|QA|
      |Reva|Dev|
